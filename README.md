# Nextcloud in Docker compose

## Installeren docker compose

### Download docker Compose:
> sudo curl -L "https://github.com/docker/compose/releases/download/1.28.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

### Verander de persmissions
> sudo chmod +x /usr/local/bin/docker-compose

## Nextcloud installeren
Ga naar https://hub.docker.com/_/nextcloud/ en zoek naar 'Base version - apache. Kopieer de docker-compose tekst. Maak nu een map op je computer. Maak hier het bestand docker-compose.yml.
Plak hier de tekst die je net hebt gekopieerd. Vul bij MYSQL_ROOT_PASSWORD en MYSQL_PASSWORD een wachtwoord in. En verander bij Ports 8080 naar 1337. 
Sla het bestand op en doe het commando:
> docker-compose up -d 

Ga nu naar localhost:1337 en rond de configuratie af.  

